using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour
{
    //Variables
    public float speed = 6.0F;
    
    private Vector3 moveDirection = Vector3.zero;

    void Update()
    {
        CharacterController controller = GetComponent<CharacterController>();
        
            
            moveDirection = new Vector3(0, Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"));
            moveDirection = transform.TransformDirection(moveDirection);
            
            moveDirection *= speed;
     
        //Making the character move
        controller.Move(moveDirection * Time.deltaTime);
    }
}