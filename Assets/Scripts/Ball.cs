using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Ball : MonoBehaviour
{
    public new Rigidbody rigidbody { get; private set; }
    public float speed = 10f;
    public Collider DeadBox;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        ResetBall();
    }

    public void ResetBall()
    {
        rigidbody.velocity = Vector3.zero;
        transform.position = new Vector3(15, 2, -2);
        if (Input.GetKeyDown(KeyCode.Space))
            Invoke(nameof(SetRandomTrajectory), 1f);
    }

    private void SetRandomTrajectory()
    {
        Vector3 force = new Vector3();
        force.z = Random.Range(-1f, 1f);
        force.x = -1f;

        rigidbody.AddForce(force.normalized * speed);
    }

    private void FixedUpdate()
    {
        rigidbody.velocity = rigidbody.velocity.normalized * speed;
        if (Input.GetKeyDown(KeyCode.Space))
            Invoke(nameof(SetRandomTrajectory), 0f);
    }

    private void OnTriggerEnter(Collider other)
    {
        ResetBall();
    }

}